solution "fgtptt"
    language "C"
    location ( _ACTION )
    targetdir( "./bin/" )
    flags { "Symbols" }
    links { "evdev" }
    configurations { "Debug" }


        -- Teamspeak 3 plugin
        project "fgtptt_plugin"
            kind "SharedLib"

            includedirs {
                "/usr/include/libevdev-1.0/libevdev/",
                "./includes/"
            }

            files {
                "./src/plugin.c",
            }


        -- Helper binary for finding input path and key presses
        project "fgtptt_helper"
            kind "ConsoleApp"

            includedirs {
                "/usr/include/libevdev-1.0/libevdev/"
            }

            files {
                "./src/helper.c",
            }
