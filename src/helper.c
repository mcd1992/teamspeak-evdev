#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
//#include <sys/stat.h>

#include <libevdev.h>

// Full path to the input device (keyboard / mouse)
#define DEVICEPATH "/dev/input/by-id/usb-Logitech_Gaming_Mouse_G502_0B8034723437-event-mouse"

struct libevdev *ev_device;
struct input_event ev_event;

static int exitloop = 0;
void sigint_handler( int code ) {
    if ( code == SIGINT ) {
        exitloop = 1;
    }
}

int main() {
    int device = open( DEVICEPATH, O_RDONLY | O_NONBLOCK );
    if ( !device || (device < 0) ) {
        printf( "ERROR %i: %s\n", device, strerror(errno) );
        return 1;
    }

    libevdev_new_from_fd( device, &ev_device );
    if ( !ev_device ) {
        printf( "ERROR %i: %s\n", ev_device, strerror(errno) );
        return 2;
    }

    signal( SIGINT, sigint_handler ); // Setup a signal handler to exit the while loop

    while( !exitloop ) {
        while ( libevdev_has_event_pending( ev_device ) ) {
            int eventcode = libevdev_next_event( ev_device, LIBEVDEV_READ_FLAG_NORMAL, &ev_event );

            if ( eventcode == LIBEVDEV_READ_STATUS_SUCCESS ) {
                unsigned int type = ev_event.type;
                unsigned int code = ev_event.code;
                int value = ev_event.value;

                const char *typestring = libevdev_event_type_get_name(type);
                const char *codestring = libevdev_event_code_get_name(type, code);

                if ( type == EV_KEY ) { // Try removing this if your event isn't showing up
                    printf( "Event:\n" );
                    printf( "\tType: %s [%i]\n", typestring, type );
                    printf( "\tCode: %s [%i]\n", codestring, code );
                    printf( "\tValue: %i\n\n", value );
                }

            } else {
                printf( "ERROR: libevdev_next_event returned %i: %s\n", eventcode, strerror(eventcode) );
            }
        }

        usleep( 100 ); // Sleep until more events come in
    }

    printf( "SIGINT received, exiting...\n" );
    libevdev_free( ev_device );
    close( device );
}
