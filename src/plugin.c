#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <libevdev.h>
#include <time.h>

#include "teamspeak/public_errors.h"
#include "teamspeak/public_errors_rare.h"
#include "teamspeak/public_definitions.h"
#include "teamspeak/public_rare_definitions.h"
#include "teamspeak/clientlib_publicdefinitions.h"
#include "teamspeak/ts3_functions.h"
#include "plugin.h"

// Number of seconds to hold the PTT key down after release
#define PTTHOLDSECONDS 0.5

// Full path to the input device (keyboard / mouse)
#define DEVICEPATH "/dev/input/by-id/usb-Logitech_Gaming_Mouse_G502_0B8034723437-event-mouse"

// Change BTN_SIDE to the key you want your PTT to trigger on. Use the helper program to find your constant name ('BTN_SIDE')
// BTN_SIDE = Mouse 4, BTN_EXTRA = Mouse 5
#define PTT_BUTTON_CODE BTN_SIDE

static struct TS3Functions ts3Functions;
static struct libevdev *ev_device;
static struct input_event ev_event;

static int exitloop = 0;
static pthread_t evdev_thread;

void *check_evdev_events() {

    static int ptt_is_down = 0; // 1 when the hardware key is depressed, 0 otherwise
    static unsigned long long ptt_release_at = 0.0; // next epoch, in ms, that the PTT will release (0.0 when not attempting auto-release)
    while( !exitloop ) {
        while ( libevdev_has_event_pending( ev_device ) ) {
            int eventcode = libevdev_next_event( ev_device, LIBEVDEV_READ_FLAG_NORMAL, &ev_event );

            if ( eventcode == LIBEVDEV_READ_STATUS_SUCCESS ) {
                unsigned int type = ev_event.type;
                unsigned int code = ev_event.code;
                int value = ev_event.value;

                const char *typestring = libevdev_event_type_get_name(type);
                const char *codestring = libevdev_event_code_get_name(type, code);

                if ( (type == EV_KEY) && (code == PTT_BUTTON_CODE) ) {
                    uint64 serverid = ts3Functions.getCurrentServerConnectionHandlerID();

                    if ( serverid ) {
                        if ( value ) {
                            //printf( "PTT Press\n" );
                            ptt_is_down = 1;

                            ts3Functions.setClientSelfVariableAsInt( serverid, CLIENT_INPUT_DEACTIVATED, INPUT_ACTIVE );
                        } else {
                            //printf( "PTT Release\n" );
                            ptt_is_down = 0;

                            struct timeval curtime;
                            gettimeofday( &curtime, NULL );
                            unsigned long long ms = (unsigned long long)(curtime.tv_sec) * 1000 + (unsigned long long)(curtime.tv_usec) / 1000;
                            ptt_release_at = ms + (PTTHOLDSECONDS * 1000);
                        }
                    }
                }

            } else {
                printf( "ERROR: libevdev_next_event returned %i: %s\n", eventcode, strerror(eventcode) );
            }

        }

        struct timeval curtime;
        gettimeofday( &curtime, NULL );
        unsigned long long ms = (unsigned long long)(curtime.tv_sec) * 1000 + (unsigned long long)(curtime.tv_usec) / 1000;
        if ( !ptt_is_down && ptt_release_at && (ptt_release_at <= ms) ) {
            //printf( "PTT Delay-Release\n" );

            uint64 serverid = ts3Functions.getCurrentServerConnectionHandlerID();
            ts3Functions.setClientSelfVariableAsInt( serverid, CLIENT_INPUT_DEACTIVATED, INPUT_DEACTIVATED );
            ptt_release_at = 0.0;
        }

        usleep( 100 ); // Sleep until more events come in
    }

    return NULL;
}

/*********************************** Required functions ************************************/
/*
 * If any of these required functions is not implemented, TS3 will refuse to load the plugin
 */

/* Unique name identifying this plugin */
const char* ts3plugin_name() {
    return "F.G.T. PTT";
}

/* Plugin version */
const char* ts3plugin_version() {
    return "1.3";
}

/* Plugin API version. Must be the same as the clients API major version, else the plugin fails to load. */
int ts3plugin_apiVersion() {
    return 20;
}

/* Plugin author */
const char* ts3plugin_author() {
    /* If you want to use wchar_t, see ts3plugin_name() on how to use */
    return "mcd1992";
}

/* Plugin description */
const char* ts3plugin_description() {
    /* If you want to use wchar_t, see ts3plugin_name() on how to use */
    return "This plugin forces a key to be bound to PTT";
}

/* Set TeamSpeak 3 callback functions */
void ts3plugin_setFunctionPointers(const struct TS3Functions funcs) {
    ts3Functions = funcs;
}

/*
 * Custom code called right after loading the plugin. Returns 0 on success, 1 on failure.
 * If the function returns 1 on failure, the plugin will be unloaded again.
 */
int ts3plugin_init() {
    /* Your plugin init code here */
    printf("F.G.T. PTT: ts3plugin_init\n");

    int device = open( DEVICEPATH, O_RDONLY | O_NONBLOCK );
    if ( !device || (device < 0) ) {
        printf( "F.G.T. PTT ERROR: Failed to open device %i: %s\n", device, strerror(errno) );
        return 1;
    }

    libevdev_new_from_fd( device, &ev_device );
    if ( !ev_device ) {
        printf( "F.G.T. PTT ERROR %i: %s\n", ev_device, strerror(errno) );
        return 2;
    }

    exitloop = 0;
    if ( pthread_create(&evdev_thread, NULL, check_evdev_events, NULL) ) {
        printf( "F.G.T. PTT ERROR: Failed to create check_evdev_events thread!\n" );
        return 1;
    }

    return 0;  /* 0 = success, 1 = failure, -2 = failure but client will not show a "failed to load" warning */
}

/* Custom code called right before the plugin is unloaded */
void ts3plugin_shutdown() {
    /* Your plugin cleanup code here */
    printf("F.G.T. PTT: ts3plugin_shutdown\n");

    exitloop = 1;
    if ( evdev_thread ) {
        if ( pthread_join(evdev_thread, NULL) ) {
            printf( "F.G.T. PTT ERROR: Failed to attach to check_evdev_events thread on shutdown!\n" );
        }

        evdev_thread = 0;
    }
}

/* Tell client if plugin offers a configuration window. If this function is not implemented, it's an assumed "does not offer" (PLUGIN_OFFERS_NO_CONFIGURE). */
int ts3plugin_offersConfigure() {
    return PLUGIN_OFFERS_NO_CONFIGURE;  /* In this case ts3plugin_configure does not need to be implemented */
}

/* Plugin command keyword. Return NULL or "" if not used. */
const char* ts3plugin_commandKeyword() {
    return "fgtptt";
}

/* Static title shown in the left column in the info frame */
const char* ts3plugin_infoTitle() {
    return "F.G.T. PTT Plugin";
}

int ts3plugin_requestAutoload() {
    return 1;  /* 1 = request autoloaded, 0 = do not request autoload */
}
